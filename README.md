# OrthoView #

Welcome! Here is my submission for the DroneDeploy coding challenge. The project is called OrthoView.

## Contents ##

The project was built on Windows using Visual Studio 2013 and OpenCV 3.1, however there is no specific windows dependent code so it can be built on any platform.

I have submitted the code, the VS solution and the [ executable ](https://bitbucket.org/lakdafari/orthoview/downloads/OrthoViewExecutable.zip)

## Build Configuration ##

Environment variable OPENCV_DIR needs to be set that points to your opencv installation (ie. D:\Development\opencv3\opencv\build\x64\vc12). The solution uses this variable to find includes and libraries.

## Command line arguments ##

The program will run as-is out of the box using defaults, however the user can customize several aspects by using command line arguments:

* **inputVideo**: provide a new file path to a new video (defaulted to ../video.mp4)
* **outputFile**: provide a file path to output the orthoview (defaulted to orthoview.jpg)
* **-sc** or **-scaleFrames**: for large video frame, provide a scaling amount (defaulted to 2)
* **-sf** or **-skipFrames**: process every *x* frames of the video (defaulted to 1 so all frames are processed)
* **-co** or **-correlation**: set the correlation coefficient threshold. If two sequential frames have a correlation below this threshold, consider the warp matrix invalid and replace it with the identity matrix so that the frame overlaps the previous one. This minimizes the effect of bad frames spoiling the mosaic. (defaulted to 0.7)
* **help**: prints the help message

Example: 
            
    OrthoView.exe ../video.mp4 -sc=4

## Results ##

Here is the result of the system using the defaults:

![Alt text](https://bitbucket.org/lakdafari/orthoview/downloads/orthoview.jpg)