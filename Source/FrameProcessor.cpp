#include <vector>
#include <iostream>
#include "FrameProcessor.h"
#include "Renderer.h"
#include "Logging.h"

using namespace std;
using namespace cv;

FrameProcessor::FrameProcessor()
{
	bNewView_ = true;
	bFrameDirty_ = false;
	fCorrThresh_ = 0.7f;
}

FrameProcessor::~FrameProcessor()
{
}

void 
FrameProcessor::configure(double fCorrThresh)
{
	fCorrThresh_ = fCorrThresh;
}

Mat
FrameProcessor::combineAffine(Mat matOne, Mat matTwo)
{
	// Copy the 2x3 affine matrices into 3x3 for composition
	Mat matFirst = Mat::eye(3, 3, CV_32F);
	Mat matSecond = Mat::eye(3, 3, CV_32F);

	matOne.row(0).copyTo(matFirst.row(0));
	matOne.row(1).copyTo(matFirst.row(1));

	matTwo.row(0).copyTo(matSecond.row(0));
	matTwo.row(1).copyTo(matSecond.row(1));

	Mat matCombo = matSecond*matFirst;

	// Return back a 2x3 for further processing
	return matCombo.rowRange(0, 2);
}

void
FrameProcessor::initialize(cv::Mat& matImgFrame, cv::Mat& matImgFrameGrey)
{
	CLOG(INFO, getModuleName()) << "Initializing for first frame";
	matImgPrevFrameGrey_ = matImgFrameGrey.clone();

	// Set the orthoview to the first frame
	int nNewHeight = matImgFrame.rows;
	int nNewWidth = matImgFrame.cols;
	matImgOrthoview_ = Mat(matImgFrame.rows, matImgFrame.cols, matImgFrame.type());
	matImgFrame.copyTo(matImgOrthoview_);

	// Set the warp to the identity matrix
	matPrevWarp_ = Mat::eye(2, 3, CV_32F);

	// Set that we are initialized
	bNewView_ = false;

	// Mark this frame as having been processed
	bFrameDirty_ = false;
}

void 
FrameProcessor::processFrame( Mat& matImgFrame )
{
	getMutex().lock();

	// Make a copy for ourselves
	matImgFrame_ = matImgFrame.clone();

	// Render the frame for visual reference
	Renderer& r = Renderer::Instance();
	r.setFrame(matImgFrame_);

	// Let the loop know we have a new frame to process
	bFrameDirty_ = true;

	getMutex().unlock();
}

Mat	
FrameProcessor::resizeViewAndCombineWarps(Mat matFrameWarp, Mat matImgFrameGrey)
{
	// Find out where the corners end up
	float m[4][3] = { { 0.0f, 0.0f, 1.0f }, { (float)matImgFrameGrey.cols, 0.0f, 1.0f }, { (float)matImgFrameGrey.cols, (float)matImgFrameGrey.rows, 1.0f }, { 0.0f, (float)matImgFrameGrey.rows, 1.0f } };
	Mat M = Mat(4, 3, CV_32F, m);
	Mat NM;
	transpose(M, NM);

	Mat matComposedAffine = combineAffine(matFrameWarp, matPrevWarp_);
	Mat transformed = matComposedAffine*NM;

	// Find the translational component to compensate for image clipping
	Mat xs = transformed.row(0);
	Mat ys = transformed.row(1);

	float* pxs = xs.ptr<float>();
	float* pys = ys.ptr<float>();

	double minx, maxx, miny, maxy;
	minMaxLoc(xs, &minx, &maxx);
	minMaxLoc(ys, &miny, &maxy);

	// How much more space do we need on the edges based on this new warped frame?
	double fExcessLeft = (max(-minx, 0.0) );
	double fExcessTop = max(-miny, 0.0);
	double fExcessRight = max(maxx - (double)matImgOrthoview_.cols, 0.0);
	double fExcessBottom = max(maxy - (double)matImgOrthoview_.rows, 0.0);

	int nExcessLeft = (int)ceil(fExcessLeft);
	int nExcessTop = (int)ceil(fExcessTop);
	int nExcessRight = (int)ceil(fExcessRight);
	int nExcessBottom = (int)ceil(fExcessBottom);

	bool bExcessLeft = nExcessLeft > 0;
	bool bExcessTop = nExcessTop > 0;
	bool bExcessRight = nExcessRight > 0;
	bool bExcessBottom = nExcessBottom > 0;

	// No image resize is necessary, just return the combined warp as-is
	if (!bExcessLeft && !bExcessTop && !bExcessRight && !bExcessBottom)
		return matComposedAffine;

	// Allocate a new image size for the screen
	int nRows = nExcessBottom + matImgOrthoview_.rows + nExcessTop;
	int nCols = nExcessRight + matImgOrthoview_.cols + nExcessLeft;

	// Now warp the frame by the translational amount
	Mat matImgOrthoviewNew = Mat::zeros(nRows, nCols, CV_8UC3);
	Rect rectRoi = Rect(nExcessLeft, nExcessTop, matImgOrthoview_.cols, matImgOrthoview_.rows);
	Mat matRoi = matImgOrthoviewNew(rectRoi);
	matImgOrthoview_.copyTo(matRoi);

	// Create the new warp that takes into account this translation
	float t[2][3] = { { 0.0f, 0.0f, (float)nExcessLeft }, { 0.0f, 0.0f, (float)nExcessTop } };
	Mat matTranslation = Mat(2, 3, CV_32F, t);
	matPrevWarp_ = matPrevWarp_ + matTranslation;

	// Set the new view
	matImgOrthoview_ = matImgOrthoviewNew.clone();

	// Compute and return the new combined warp for further processing
	return combineAffine(matFrameWarp, matPrevWarp_);
}

void
FrameProcessor::threadLoop()
{
	while (true)
	{
		getMutex().lock();

		if (bFrameDirty_)
		{
			CLOG(INFO, getModuleName()) << "Processing";

			// Convert the frame to greyscale for alignment computation
			Mat matImgFrameGrey;
			cvtColor(matImgFrame_, matImgFrameGrey, CV_BGR2GRAY);

			// This is the first frame, initialize
			if (bNewView_)
			{
				initialize(matImgFrame_, matImgFrameGrey);
				getMutex().unlock();
				continue;
			}

			// Find the affine warping that aligns the current matImgFrameGrey onto the previous one.
			cv::setBreakOnError(true);
			Mat matFrameWarp = Mat::eye(2, 3, CV_32F);
			double ret = findTransformECC(matImgFrameGrey, matImgPrevFrameGrey_, matFrameWarp, MOTION_EUCLIDEAN, TermCriteria(TermCriteria::COUNT + TermCriteria::EPS, 50, 0.0001));
			if (ret < fCorrThresh_)
			{
				// Correlation is low, so just use the identity warp so the tracking doesn't get completely off track
				CLOG(WARNING, getModuleName()) << "Correlation is below " << fCorrThresh_ << ", using identity warp";
				matFrameWarp = Mat::eye(2, 3, CV_32F);
			}

			CLOG(INFO, getModuleName()) << "Corr: " << ret;

			// Resize the view and return the composed affine warp to apply to this frame
			Mat matComposedAffine = resizeViewAndCombineWarps(matFrameWarp, matImgFrameGrey);

			// Determine the mask for copying, using BORDER_REFLECT to elimiate the jagged borders
			Mat matImgFrameComposedWarp(matImgOrthoview_.size(), matImgOrthoview_.type());
			warpAffine(matImgFrame_, matImgFrameComposedWarp, matComposedAffine, matImgFrameComposedWarp.size(), INTER_CUBIC);
			Mat matComposedMask = matImgFrameComposedWarp.clone();

			// Warp the image into the right position
			warpAffine(matImgFrame_, matImgFrameComposedWarp, matComposedAffine, matImgFrameComposedWarp.size(), INTER_CUBIC, BORDER_REFLECT);

			// Copy the newly combo warped matImgFrame onto the mosaic
			matImgFrameComposedWarp.copyTo(matImgOrthoview_, matComposedMask);

			// Render the mosaic to the screen
			Renderer& r = Renderer::Instance();
			r.setOrthoview(matImgOrthoview_);

			// Next step updates
			matPrevWarp_ = matComposedAffine.clone();
			matImgPrevFrameGrey_ = matImgFrameGrey.clone();

			// Mark this frame as having been processed
			bFrameDirty_ = false;
		}

		getMutex().unlock();

		// Some breathing room
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}