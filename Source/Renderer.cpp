#include "Renderer.h"
#include "Logging.h"

using namespace std;
using namespace cv;


Renderer::Renderer()
{
	bFrameDirty_ = false;
	bOrthoViewDirty_ = false;
}


Renderer::~Renderer()
{
}

void 
Renderer::configure(std::string strFilePath)
{
	strOutputFilePath_ = strFilePath;
	CLOG(INFO, getModuleName()) << "Configuration set " << strOutputFilePath_ << " (file path)";
}

void
Renderer::setFrame(cv::Mat matImg)
{
	getMutex().lock();

	matImgFrame_ = matImg.clone();
	bFrameDirty_ = true;

	getMutex().unlock();
}

void
Renderer::setOrthoview(Mat matImg)
{
	getMutex().lock();

	matImgOrthoview_ = matImg.clone();
	bOrthoViewDirty_ = true;

	getMutex().unlock();
}

void
Renderer::threadLoop()
{
	while (true)
	{
		getMutex().lock();

		if (bFrameDirty_)
		{
			// Display a new frame
			imshow("frame", matImgFrame_);
			waitKey(20);
			
			bFrameDirty_ = false;
		}

		if (bOrthoViewDirty_)
		{
			// Display a new view
			imshow("orthoview", matImgOrthoview_);
			waitKey(20);

			// Save out the file
			imwrite(strOutputFilePath_, matImgOrthoview_);

			bOrthoViewDirty_ = false;
		}

		getMutex().unlock();

		std::this_thread::sleep_for(std::chrono::milliseconds( 100 ));
	}
}