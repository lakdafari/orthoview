#pragma once

#include <thread>
#include <mutex>

template<class T>
class OvModule
{
	public:
		OvModule();
		virtual ~OvModule();

		static T&	Instance();
		virtual void initializeModule();

		void			start();
		std::thread&	getThread();

	protected:

		virtual const char* getModuleName() = 0;

		virtual void	threadLoop() = 0;
		std::mutex&		getMutex();

	private:

		static T*	pInstance_;

		std::mutex		lock_;
		std::thread*	pThisThread_;
};

template <class T>
std::thread&
OvModule<T>::getThread()
{
	return *pThisThread_;
}

template <class T>
std::mutex&
OvModule<T>::getMutex()
{
	return lock_;
}

template <class T>
OvModule<T>::OvModule()
	: pThisThread_(0)
{
}


template <class T>
OvModule<T>::~OvModule()
{
}

template <class T>
void
OvModule<T>::start()
{
	pThisThread_ = new thread(&OvModule<T>::threadLoop, this);
}

template <class T>
void
OvModule<T>::initializeModule()
{
	// Create a logger for this module
	el::Loggers::getLogger(getModuleName());
	el::Loggers::reconfigureLogger(getModuleName(), loggerConfig);
}

template<class T>
T&
OvModule<T>::Instance()
{
	if (!pInstance_)
	{
		pInstance_ = new T();
		pInstance_->initializeModule();
	}

	return *pInstance_;
}

template <class T> 
T* OvModule<T>::pInstance_ = 0;
