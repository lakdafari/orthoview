#pragma once

#include <string>
#include <thread>

#include "OvModule.h"

class FrameFeeder : public OvModule<FrameFeeder>
{
	public:
		FrameFeeder();
		virtual ~FrameFeeder();

		void configure(std::string strFilePath, int nScaleFrame, int nSkipFrame);

	protected:

		const char*	getModuleName();
		void		threadLoop();

	private:

		std::string strVideoFilePath_;
		int nSkipFrame_;
		int nScaleFrame_;
};

inline const char* 
FrameFeeder::getModuleName()
{
	return "FrameFeeder";
}
