#pragma once

#include "opencv2/opencv.hpp"
#include <thread>

#include "OvModule.h"

class FrameProcessor : public OvModule<FrameProcessor>
{
	public:
		FrameProcessor();
		virtual ~FrameProcessor();

		void configure(double fCorrThresh);

		void processFrame(cv::Mat& matImgFrame);

	protected:

		const char*	getModuleName();
		void		threadLoop();

		void	initialize(cv::Mat& matImgFrame, cv::Mat& matImgFrameGrey);
		cv::Mat combineAffine(cv::Mat matOne, cv::Mat matTwo);
		cv::Mat	resizeViewAndCombineWarps(cv::Mat matComposedAffine, cv::Mat matImgFrameGrey);

	private:

		bool bFrameDirty_;
		cv::Mat matImgFrame_; // Current frame

		bool	bNewView_;
		cv::Mat matImgOrthoview_;		// The full mosaic
		cv::Mat matImgPrevFrameGrey_;	// For alignment
		cv::Mat matPrevWarp_;			// Used as a reference to warp into mosaic

		double fCorrThresh_; // Frame aligmnment correlation below this will cause us the reject this frame's warp and place it on top of the previous frame (to avoid tracking getting lost)
};

inline const char*
FrameProcessor::getModuleName()
{
	return "FrameProcessor";
}
