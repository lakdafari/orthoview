#include "FrameFeeder.h"
#include "opencv2/opencv.hpp"
#include "FrameProcessor.h"
#include "Logging.h"

using namespace cv;
using namespace std;


FrameFeeder::FrameFeeder()
{
}

FrameFeeder::~FrameFeeder()
{
}

void 
FrameFeeder::configure(std::string strFilePath, int nScaleFrame, int nSkipFrame)
{
	strVideoFilePath_ = strFilePath;
	nSkipFrame_ = nSkipFrame;
	nScaleFrame_ = nScaleFrame;

	CLOG(INFO, getModuleName()) << "Configuration set " << strVideoFilePath_ << " (file path), " << nSkipFrame_ << " (skip frame), " << nScaleFrame_ << " (scale frame)";
}

void
FrameFeeder::threadLoop()
{
	// Start reading file
	VideoCapture cap(strVideoFilePath_);
	if (!cap.isOpened())
	{
		CLOG(ERROR, getModuleName()) << "Cannot open video file";
		return;
	}

	FrameProcessor& fp = FrameProcessor::Instance();

	// Start reading frames and send them over to be processed
	int nCounter = 0;
	Mat matImgFrame;
	while (true)
	{
		cap >> matImgFrame;
		if (matImgFrame.empty())
			break;

		if (nCounter % nSkipFrame_ != 0)
		{
			++nCounter;
			continue;
		}

		// Scale the image for visualization
		Size sizeFrame(matImgFrame.size());
		sizeFrame.width = sizeFrame.width / nScaleFrame_;
		sizeFrame.height = sizeFrame.height / nScaleFrame_;

		Mat matImgFrameResized;
		resize(matImgFrame, matImgFrameResized, sizeFrame);

		// Send for processing
		CLOG(INFO, getModuleName()) << "Processing frame " << nCounter;
		fp.processFrame(matImgFrameResized);

		++nCounter;

		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}