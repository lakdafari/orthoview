#pragma once

#include "opencv2/opencv.hpp"
#include <thread>
#include <mutex>
#include "OvModule.h"

class Renderer : public OvModule<Renderer>
{
	public:

		Renderer();
		virtual ~Renderer();

		void configure(std::string strFilePath);

		void setFrame(cv::Mat matImg);
		void setOrthoview(cv::Mat matImg);

	protected:

		const char*	getModuleName();
		void		threadLoop();

	private:

		std::string strOutputFilePath_;

		bool	bFrameDirty_;
		cv::Mat matImgFrame_;

		bool	bOrthoViewDirty_;
		cv::Mat matImgOrthoview_;
};

inline const char*
Renderer::getModuleName()
{
	return "Renderer";
}
