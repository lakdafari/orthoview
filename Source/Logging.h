#pragma once

#define ELPP_NO_DEFAULT_LOG_FILE
#define ELPP_THREAD_SAFE
#define ELPP_FORCE_USE_STD_THREAD
#include "easylogging++.h"

extern el::Configurations loggerConfig;
