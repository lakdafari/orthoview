#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include <thread>

#include "FrameFeeder.h"
#include "FrameProcessor.h"
#include "Renderer.h"

#include "Logging.h"

INITIALIZE_EASYLOGGINGPP

el::Configurations loggerConfig;

using namespace cv;
using namespace std;

void printHelp()
{
	cout << "\nUsage example: \n./orthoview ../data/video.mp4 -sc=4 -sf=5 -co=0.7\n" << endl;
}

int main(int argc, char** argv)
{
	// Logging configuration
	loggerConfig.setToDefault();
	loggerConfig.setGlobally(el::ConfigurationType::Filename, "orthoview.log");
	el::Loggers::getLogger("Main");
	el::Loggers::reconfigureLogger("Main", loggerConfig);

	const std::string keys =
		"{@inputVideo|../video.mp4|input video filename }"
		"{@outputFile|orthoview.jpg|output image filename}"
		"{sf skipFrames|1| Process every x frames }"
		"{sc scaleFrames|2| Divide frame size }"
		"{co correlation|0.7| Minimum threshold for frame rejection }"
		"{h help||print help message }"
		;

	CommandLineParser parser(argc, argv, keys);
	parser.about("OrthoView");

	if (parser.has("help"))
	{
		parser.printMessage();
		printHelp();
		return 1;
	}

	// Grab the parameters from the command line
	string strInput = parser.get<string>(0);
	string strOuput = parser.get<string>(1);

	int nSkipFrame = parser.get<int>("sf");
	int nScaleFrame = parser.get<int>("sc");
	double fCorrThresh = parser.get<double>("co");

	if (nScaleFrame < 1)
	{
		CLOG(INFO, "Main") << "Invalid scale frame size";
		printHelp();
		return 1;
	}

	if (nSkipFrame < 1)
	{
		CLOG(INFO, "Main") << "Invalid skip frame size";
		printHelp();
		return 1;
	}

	CLOG(INFO, "Main") << "OrthoView started";

	// Start and configuire the frame processor
	CLOG(INFO, "Main") << "Starting FrameProcessor";
	FrameProcessor& fp = FrameProcessor::Instance();
	fp.configure(fCorrThresh);
	fp.start();

	// Start and configuire the frame processor
	CLOG(INFO, "Main") << "Starting Renderer";
	Renderer& rend = Renderer::Instance();
	rend.configure( strOuput );
	rend.start();

	// Start processing
	FrameFeeder& ff = FrameFeeder::Instance();
	CLOG(INFO, "Main") << "Starting FrameFeeder";
	ff.configure(strInput, nScaleFrame, nSkipFrame);

	CLOG(INFO, "Main") << "Begin processing";
	ff.start();

	// Wait for the feeder to be done
	ff.getThread().join();

	CLOG(INFO, "Main") << "Feed complete";

	waitKey();

	return 0;
}
